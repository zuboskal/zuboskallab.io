---
title: 'кто был никто'
date: '2019-09-25T00:00:00Z'
hero: /images/8.jpg
summary: 'В «Мертвеце» Джима Джармуша индеец Никто объясняет робкому бухгалтеру Биллу Блейку, что тот на самом деле великий поэт и убийца (сам-то Блейк знает..'
---

В «Мертвеце» Джима Джармуша индеец Никто объясняет робкому бухгалтеру Биллу Блейку, что тот на самом деле великий поэт и убийца (сам-то Блейк знает, что он всего лишь приехал просить работу счетовода в конторе, потому что его бросила девушка). По ходу действия, с каждым новым убийством, бухгалтер Блейк всё больше становится похож на того, кем его воображают другие — самим собой.

Никто из нас не объявлял инженера Молчанова великим писателем. Он и правда инженер, и вовсе не писатель (впрочем, в 34-м году эта профессия максимально приближена к инженерской).

Но кто он, Молчанов, если не писатель, если его в уголовном суде судят «за литературу»? Если его рассказы разбирает следователь — как критик? Если его журнал после смерти готовы издать тиражом в 25 раз больше прижизненного?

Если так пойдёт и дальше, мы рискуем дожить до открытия в Салтыковке памятного бюста. Таблички на доме. Переименования дачной библиотеки в «имени Молчанова». Что может быть смешнее?

(а кто был никем — тот станет Джимом Джармушем)
