---
title: about
date: 2019-12-01T00:00:00.000+00:00
background_color: '#B4CEC8'
taglines:
  union: zuboskal@memo.ru
type: ''
---

{{<beginSection>}}

## Письма подписчикам журнала «Зубоскал», которого нет.

## Присоединиться к ним или прийти с вопросами и комментариями к авторам можно по адресу:

## [zuboskal@memo.ru](malto:zuboskal@memo.ru)

{{<endSection>}}
{{<beginSection class="end">}}

## союз беспечальных коптителей салтыковского неба.

{{<endSection>}}
